package com.eloancn.leecode.classic;

import java.util.HashSet;
import java.util.Set;

/**
 * 滑动窗口算法
 * 给一组大小为n的整数数组，计算长度为k的子数组的最大值
比如：数组{1,2,3,4,5,7,6,1,8},k=2，那么最终结果应该是7+6=13最大。
 * @author lihepeng
 *
 */
public class SlideWindow {
	
	/**
	 *   找相匹配的最大的字串
	 *   给定一个字符串 s 和一些长度相同的单词 words。找出 s 中恰好可以由 words 中所有单词串联形成的子串的起始位置。
			注意子串要与 words 中的单词完全匹配，中间不能有其他字符	
	 * @return
	 */
	public static int findSubstring(String s,String[] words) {
			
			return 0;
	}
	/**
	 * 不重复的最长的字串
	 */
	public int lengthOfLongestSubstring(String s) {
		Set<Character> set = new HashSet<>();
		
		return 0;
	}
	/**
	 * 数量为k的数组最大值
	 * @param array
	 * @param k
	 * @return
	 */
	public static int maxnum(int[] array,int k){
		if (k > array.length) {
			return -1;
		}
		int sum =0;
		for(int i=0;i<k;i++) {
			sum+=array[i];
		}
		int left = k;
		int tempSum = sum;
//		while(left+k<array.length) {
//			tempSum = tempSum-array[left]+array[left+k];
//			left++;
//			sum = sum>tempSum?sum:tempSum;
//		}
		while(left<array.length) {
			tempSum = tempSum +array[left] - array[left-k];;
			left++;
			sum = sum>tempSum?sum:tempSum;
		}
		
		return sum;
	}
	public static void main(String[] args) {
		int[] arr={1, 4, 2, 10, 2, 3, 1, 0, 20};
		int k=2;
		System.out.println(maxnum(arr,k));
	}
	
}

