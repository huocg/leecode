package com.eloancn.leecode.mulitithread;
/**
 * 我们提供一个类：

class FooBar {
  public void foo() {
    for (int i = 0; i < n; i++) {
      print("foo");
    }
  }

  public void bar() {
    for (int i = 0; i < n; i++) {
      print("bar");
    }
  }
}
两个不同的线程将会共用一个 FooBar 实例。其中一个线程将会调用 foo() 方法，另一个线程将会调用 bar() 方法。

请设计修改程序，以确保 "foobar" 被输出 n 次。

 

示例 1:

输入: n = 1
输出: "foobar"
解释: 这里有两个线程被异步启动。其中一个调用 foo() 方法, 另一个调用 bar() 方法，"foobar" 将被输出一次。
示例 2:

输入: n = 2
输出: "foobarfoobar"
解释: "foobar" 将被输出两次。

来源：力扣（LeetCode）
链接：https://leetcode-cn.com/problems/print-foobar-alternately
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author eloancn
 *
 */
class FooBar {
    private int n;
    // 对象锁，用于两个线程之间的数据同步
    private String lock= "lock";
    private static int count =0;
    public FooBar(int n) {
        this.n = n;
    }

    public void foo(Runnable printFoo) throws InterruptedException {
        
        for (int i = 0; i < n; i++) {
            synchronized (lock) {
				if (count % 2 == 1 ) {
					lock.wait();
				}
				
				printFoo.run();
				count ++;
				lock.notifyAll();
			}
        }
    }

    public void bar(Runnable printBar) throws InterruptedException {
        
        for (int i = 0; i < n; i++) {
        	synchronized (lock) {
				if (count % 2 ==0) {
					lock.wait();
				}
				printBar.run();
				count++;
				lock.notifyAll();
			}
            
        	printBar.run();
        }
    }
}
