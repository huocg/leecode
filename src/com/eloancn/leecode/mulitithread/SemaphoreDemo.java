package com.eloancn.leecode.mulitithread;

import org.junit.Test;

import java.util.Random;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;


/**
 * 
 * @author eloancn
 *	使用信号量模拟学生打饭的流程
 *   存在两个窗口 学生可以在两个窗口去打饭，有三种情况，其一学会很有耐心，等到打饭完成
 *   其二学生没有耐心等了一会就放弃了，其三学生很有耐心但是一段时间之后行政命令通知聚餐
 *   
 */
public class SemaphoreDemo {
	@Test
	public void runTest() {
		// 两个打饭窗口 
		Semaphore semaphore = new Semaphore(2, true);
	
	}

}
class Student implements Runnable{
	private Semaphore semaphore;
	// 打饭许可
	private String name;
	
	private int type;
	Random ran = new Random();
	
	
	@Override
	public void run() {
		switch (type) {
		case 0:
			// 获得一个打饭许可，总共有两个
			semaphore.acquireUninterruptibly();
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			semaphore.release();
			break;
		case 1:
			// 等了一会放弃情况
			/*
			 * if (semaphore.tryAcquire(ran.nextInt(16000), TimeUnit.MILLISECONDS)) {
			 * 
			 * }
			 */
				
		default:
			break;
		}
	}


	public Student(Semaphore semaphore, String name, int type) {
		super();
		this.semaphore = semaphore;
		this.name = name;
		this.type = type;
	}
	
}
