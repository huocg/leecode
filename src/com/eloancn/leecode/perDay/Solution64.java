package com.eloancn.leecode.perDay;
/**
 * 求 1+2+...+n ，
 * 要求不能使用乘除法、for、while、if、else、switch、case
 * 等关键字及条件判断语句（A?B:C）。
 * @author lihepeng
 *
 */

import org.junit.Test;

public class Solution64 {
	//  使用&& 的短路性质来判断是不是递归结束
	
	public int sumNums(int n) {
			//return n == 0?0:n+sumNums(n-1);
				 boolean flag = n >0&&(n+=sumNums(n-1))>0;
				 return n;
	}
	
	@Test
	public void runTest01() {
		System.out.println(this.sumNums(10));
	}
	
}
