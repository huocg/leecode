package com.eloancn.leecode.string;

import org.junit.Test;

/**
 * 实现 strStr() 函数。

给定一个 haystack 字符串和一个 needle 字符串，在 haystack 字符串中找出 needle 字符串出现的第一个位置 (从0开始)。如果不存在，则返回  -1。

来源：力扣（LeetCode）
链接：https://leetcode-cn.com/problems/implement-strstr
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author lihepeng
 *
 */


public class Solution28 {
	public int strStr(String haystack, String needle) {
		if (needle==null||needle=="") {
			return 0;
		}
		int l = haystack.length();
		int n = needle.length();
		for(int start =0;start<l-n+1;++start) {
			if (haystack.substring(start,start+n).equals(needle)) {
				return start;
			}
		}
		return -1;
    }
	/**
	 * 优化，不需要字符串全部比较 双指针，当两个字符串有一个不相同的时候
	 * 终止比较
	 * @param haystack
	 * @param needle
	 * @return
	 */
	public int strStr2(String haystack, String needle) {
		if (needle==null||needle=="") {
			return 0;
		}
		int l = haystack.length();
		int n = needle.length();
		for(int start =0;start<l-n+1;++start) {
			/*
			 * if (haystack.substring(start,start+n).equals(needle)) { return start; }
			 */
			String substring = haystack.substring(start,start+n);
			for(int i=0;i<substring.length();i++) {
				if (substring.charAt(i)==needle.charAt(i)) {
					break;
				}
			}
		}
		return -1;
    }
	
	@Test
	public void runTest() {
		int strStr = this.strStr("hello", "ll");
		System.out.println(strStr);
	}
}
