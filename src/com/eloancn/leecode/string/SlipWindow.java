package com.eloancn.leecode.string;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

/**
 * 问题描述：给定一个字符串，请你找出其中不含有重复字符的 最长子串 的长度。
	输入: “abcabcbb”
	输出: 3
 * @author lihepeng
 *
 */
public class SlipWindow {
	public int lengthOfLongestSubstring(String s) {
		Set<Character> set = new HashSet<>();
		// i 左边   j 为右边
		int ans =0,i=0,j=0;
		while(i<s.length()&& j<s.length()) {
			if (!set.contains(s.charAt(j))) {
				set.add(s.charAt(j));
					j++;
				ans = Math.max(ans, j-i);
			}else {
				set.remove(s.charAt(i));
				i++;
			}
		}
		return ans;
	}
	@Test
	public void runTest() {
		int lengthOfLongestSubstring = this.lengthOfLongestSubstring("aabcdeff");
		System.out.println(lengthOfLongestSubstring);
	}
}
