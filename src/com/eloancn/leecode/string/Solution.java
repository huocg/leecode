package com.eloancn.leecode.string;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Solution {
	public boolean isUnique(String astr) {
		Set set = new HashSet<>();
		for(int i=0;i<astr.length();i++) {
			set.add(astr.charAt(i));
		}
		return astr.length() == set.size();

    }

}
