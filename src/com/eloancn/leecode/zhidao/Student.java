package com.eloancn.leecode.zhidao;


public class Student {
    private String studentNo;
    private String studentName;
    private Integer englishScore;
    private Integer mathScore;
    private Integer javaScore;
    private Integer sumScore;

    public Student() {

    }

    public Student(String studentNo, String studentName, Integer englishScore, Integer mathScore, Integer javaScore) {
        this.studentNo = studentNo;
        this.studentName = studentName;
        this.englishScore = englishScore;
        this.mathScore = mathScore;
        this.javaScore = javaScore;
        this.sumScore = this.englishScore+this.mathScore+this.javaScore;
    }

    public Integer sum(){
        return this.sumScore;
    }
    public Integer avageScore(){
        return this.sumScore/3;
    }
    public void showStudentInfo(){
        System.out.println("学号："+this.studentNo+" 学生名字："+
                this.studentName+" 英语成绩:"+englishScore
                +" java 成绩:"+javaScore+" 数学成绩:"+mathScore
                +" 总成绩："+this.sumScore+" 平均成绩："+this.avageScore());
    }

    public static void main(String[] args) {
        Student zhangsan = new Student("1", "张三", 110, 50, 100);
        zhangsan.showStudentInfo();
    }

}
