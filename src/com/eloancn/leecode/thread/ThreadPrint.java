package com.eloancn.leecode.thread;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class ThreadPrint {
    private int count;
    public void thread(){
        Thread t1 = new Thread(()->{
            while(true){
                count++;
                server1();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"t1"){};

        t1.start();
    }
    public void server1(){
        if (count>10){
            System.out.println("执行到达上限！");
        }else {

            System.out.println(Thread.currentThread().getName()+"第"+count+"次被调用");
        }

    }
    @Test
    public void runTest(){
        thread();
        try {
            Thread.currentThread().join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
