package com.eloancn.leecode.tree;

public class BinaryTreeImpl<T> implements BinaryTree<T> {
	private Node<T> rootNode;
	
	public BinaryTreeImpl(Node<T> rootNode) {
		super();
		this.rootNode = rootNode;
	}

	@Override
	public boolean isEmpty() {
		
		return rootNode == null;
	}

	@Override
	public int size(Node<T> root) {
		if (rootNode == null) {
			return 0;
		}
		int n1 = this.size(rootNode.leftChild);
		int n2 = this.size(rootNode.rightChild);
		return n1+n2+1;
	}

	@Override
	public int getHeigh(Node<T> root) {
		if (root == null) {
			return 0;
		}
		if (root.leftChild ==null && root.rightChild == null) {
			return 1;
		}
		return Math.max((getHeigh(root.leftChild)),getHeigh(root.rightChild));
	}

	@Override
	public Node<T> findKey() {
		
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void preOrderTravel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inOrderTravel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterOrderTravel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void levelOrder() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void lookByLeft() {
		// TODO Auto-generated method stub
		
	}

}
