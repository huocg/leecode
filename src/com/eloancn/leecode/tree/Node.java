package com.eloancn.leecode.tree;
/**
 * 二叉树节点
 * @author lihepeng
 *
 */
public class Node<T> {
	T value;
	Node leftChild;
	Node rightChild;
	public Node(T value) {
		super();
		this.value = value;
	}
	
}
