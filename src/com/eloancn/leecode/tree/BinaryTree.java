package com.eloancn.leecode.tree;
/**
 * 二叉树业务类
 * @author lihepeng
 *
 */
public interface BinaryTree<T> {
	// 是否为空节点
	public boolean 	isEmpty();
	// 返回节点数量
	public int size(Node<T>  root);
	// 返回二叉树高度
	public int getHeigh(Node<T> root);
	//  查询节点
	public Node findKey();
	// 前序遍历
	public void preOrderTravel();
	// 中序遍历
	public void inOrderTravel();
	// 后续遍历
	public void afterOrderTravel();
	// 层级遍历
	public void levelOrder();
	// 从左向右看到节点
	public void lookByLeft();
	
}
