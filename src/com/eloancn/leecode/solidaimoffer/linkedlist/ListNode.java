package com.eloancn.leecode.solidaimoffer.linkedlist;

public class ListNode {
    int value;
    ListNode next;
    public ListNode(int value) {
        this.value = value;
    }

}
