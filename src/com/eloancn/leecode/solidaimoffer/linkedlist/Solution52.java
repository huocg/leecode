package com.eloancn.leecode.solidaimoffer.linkedlist;

/**
 * 输入两个链表，找出它们的第一个公共节点。
 */
public class Solution52 {
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        if (headA ==null || headB ==null) {
            return null;
        }

        ListNode node1 = headA;
        ListNode node2 = headB;
        while(node1!=node2){
            // 你走我的路，我走你的路相遇的节点就是共同的节点

            node1 = node1==null?headB:node1.next;
            node2 = node2 == null?headA:node2.next;
        }
        return node1;
    }
}
