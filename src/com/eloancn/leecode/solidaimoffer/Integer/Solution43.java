package com.eloancn.leecode.solidaimoffer.Integer;

import org.junit.Test;

/**
 * 输入一个整数 n ，求1～n这n个整数的十进制表示中1出现的次数。
 *
 * 例如，输入12，1～12这些整数中包含1 的数字有1、10、11和12，1一共出现了5次。
 * 示例 1：
 * 输入：n = 12
 * 输出：5
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/1nzheng-shu-zhong-1chu-xian-de-ci-shu-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution43 {
    // 通过遍历，然后除以10 判断是不是等于1 来做累加
    public int countDigitOne(int n) {
        int countDigitOne = 0;
        for(int i=1;i<=n;i++){
            int current = i;
            while(current / 10!=0){
                if (current%10==1){
                    countDigitOne++;
                    break;
                }
                current=current/10;
            }
        }

        return countDigitOne;
    }
    @Test
    public void runTest() {
        int i = countDigitOne(12);
        System.out.println(i);
    }
}
