package com.eloancn.leecode.solidaimoffer.array;

/**
 * 统计一个数字在排序数组中出现的次数。
 *
 *
 *
 * 示例 1:
 *
 * 输入: nums = [5,7,7,8,8,10], target = 8
 * 输出: 2
 */
public class Solution53 {
    /**
     * o(n) 解法
     * @param nums
     * @param target
     * @return
     */
    public int search(int[] nums, int target) {
        int count = 0;
        for(int num:nums) {
            if (num == target){
                count++;
            }
        }
        return count;
    }

    /**
     * 使用两次 二分法方式 求解
     * 确定两个边界，然后取差便是
     * @param nums
     * @param target
     * @return
     */
    public int search01(int []nums , int target) {
        int i=0,j=nums.length -1;
        while(i<=j){
            int m = (i+j)/2;
            if (nums[m] <target) {
                i = m+1;
            }else {
                j= m-1;
            }
        }

        return 0;
    }

}
