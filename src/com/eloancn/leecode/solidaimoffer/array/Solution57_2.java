package com.eloancn.leecode.solidaimoffer.array;

import java.util.ArrayList;
import java.util.List;

/**
 * 输入一个正整数 target ，输出所有和为 target 的连续正整数序列（至少含有两个数）。
 *
 * 序列内的数字由小到大排列，不同序列按照首个数字从小到大排列。
 * 示例 1：
 * 输入：target = 9
 * 输出：[[2,3,4],[4,5]]
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/he-wei-sde-lian-xu-zheng-shu-xu-lie-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution57_2 {
    /**
     * 使用滑动窗口方法来处理 连续数组问题
     * @param target
     * @return
     */
    public int[][] findContinuousSequence(int target) {
        int i =1;
        int j =1;
        // 窗口中的数字之和
        int sum =0;
        List<int[]> res = new ArrayList<>();
        while(i<=target/2){
            if (sum<target){
                // 右边界向右移动
                sum+=j;
                j++;
            }else if (sum>target){
                sum -=i;
                i++;
            }else {
                int[] arr = new int[j-i];
                for (int k=i;k<j;k++){
                    arr[k-i] = k;
                }
                res.add(arr);
                sum -=i;
                i++;
            }
        }
        return res.toArray(new int[res.size()][]);
    }
}
