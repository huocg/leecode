package com.eloancn.leecode.solidaimoffer.array;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;

/**
 * 输入一个递增排序的数组和一个数字s，在数组中查找两个数，使得它们的和正好是s。如果有多对数字的和等于s，则输出任意一对即可。
 *
 *  
 *
 * 示例 1：
 *
 * 输入：nums = [2,7,11,15], target = 9
 * 输出：[2,7] 或者 [7,2]
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/he-wei-sde-liang-ge-shu-zi-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution57 {
    public int[] twoSum(int[] nums, int target) {
        int [] res = new int[2];
        HashMap<Integer,Integer> map = new HashMap();
        for(int num :nums){
            if (map.containsKey(target -num)){
                res[0]=num;
                res[1]=target - num;
                return res;
            }
            map.put(num,target-num);
        }
        return null;
    }
    @Test
    public void runTest(){
        int [] nums = {2,7,11,15};
        int[] ints = twoSum(nums, 9);
        System.out.println(Arrays.toString(ints));

    }
}
