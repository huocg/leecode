package com.eloancn.leecode.solidaimoffer.string;

import java.util.HashSet;
import java.util.Set;

/**
 * 请从字符串中找出一个最长的不包含重复字符的子字符串，计算该最长子字符串的长度。
 *
 *  
 *
 * 示例 1:
 *
 * 输入: "abcabcbb"
 * 输出: 3
 * 解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/zui-chang-bu-han-zhong-fu-zi-fu-de-zi-zi-fu-chuan-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 *
 */
public class Solution48 {
    /**
     * 活动窗口法
     *
     * @param s
     * @return
     */
    public int lengthOfLongestSubstring(String s) {
        Set<Character> set = new HashSet<>();
        int left = 0,right = 0,res =0;
        while(right <s.length()) {
           char c =  s.charAt(right ++);
           // 存在重复的字符，移动左指针，直到滑动窗口不包含该字符
            while(set.contains(c)) {
                set.remove(s.charAt(left ++ ));
            }
            set.add(c);
            res = Math.max(res,right - left);
        }
        System.out.println(set);
        return res;

    }
}
