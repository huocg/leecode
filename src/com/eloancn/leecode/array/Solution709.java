package com.eloancn.leecode.array;

import org.junit.Test;

/**
 * 实现函数 ToLowerCase()，该函数接收一个字符串参数 str，
 * 并将该字符串中的大写字母转换成小写字母，之后返回新的字符串。
 * @author eloancn
 *
 */
public class Solution709 {
	public String toLowerCase(String str) {
		char[] charArray = str.toCharArray();
		StringBuffer sb = new StringBuffer();
		
		for (int i = 0; i < charArray.length; i++) {
			if (charArray[i]>=65&&charArray[i]<=90) {
				
				sb.append((char)(charArray[i]+32));
			}else {
				sb.append(charArray[i]);
			}
		}
		return sb.toString();
    }
	@Test
	public void runTest() {
		char s1 = 'z';
		char s2 = 'Z';
		System.out.println((int)s1);
		System.out.println((int)s2);
	}
	@Test
	public void runTest01() {
		System.out.println(toLowerCase("HeLLO"));
	}

}
