package com.eloancn.leecode.sort;


import org.junit.Test;

/**
 * @author eloancn
 */
public class BubbleSort {
	public void sort(int[] arr) {
		if (arr.length == 0) {
			return ;
		}
		for(int i=0;i<arr.length;i++) {
			for(int j=0;j<arr.length-1;j++) {
				if(arr[j]>arr[j+1]) {
					int tem = arr[j+1];
					arr[j+1] = arr[j];
					arr[j] = tem;
				}
			}
		}
	}
	
	@Test
	public void runTest() {
		int[] arr= {2,1,7,4,3,5,6};
		for (int i : arr) {
			System.out.print(i);
		}
		System.out.println("   ");
		sort(arr);
		for (int i : arr) {
			System.out.print(i);
		}
	}

}
