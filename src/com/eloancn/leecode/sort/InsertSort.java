package com.eloancn.leecode.sort;
/**
 * 
 * @author lihepeng
 *
 */



public class InsertSort {
	public static void insertSort(int []arr) {
		for(int i =1 ;i<arr.length ; i++) {
			for(int j =i ;j>0 ;j--) {
				if(arr [j]<arr[j-1]) {
					int temp = arr[j];
					arr[j] = arr[j-1];
					arr[j-1] = temp;
				}
			}
		}
	}
	
	public static void main(String[] args) {
		int[] arr = {1,4,2,6,5,7,9};
		insertSort(arr); 
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i]);
		}
	}

}
