package com.eloancn.leecode.solid_aim_offer.array;

/**
 * 写一个函数，输入 n ，求斐波那契（Fibonacci）数列的第 n 项。斐波那契数列的定义如下：
 *
 * F(0) = 0,   F(1) = 1
 * F(N) = F(N - 1) + F(N - 2), 其中 N > 1.
 * 斐波那契数列由 0 和 1 开始，之后的斐波那契数就是由之前的两数相加而得出。
 *
 * 答案需要取模 1e9+7（1000000007），如计算初始结果为：1000000008，请返回 1。
 *
 *  
 *
 * 示例 1：
 *
 * 输入：n = 2
 * 输出：1
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/fei-bo-na-qi-shu-lie-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution10 {
    /**
     * 使用递归的方案完成
     * @param n
     * @return
     */
    public int fib(int n) {
        if (n<2){
            return n;
        }
        return fib(n-1)+fib(n-2);
    }

    /**
     * 使用动态规划的方案完成
     * n =0 console 0
     * n=1 console 1
     * n=2 console 1+0;
     * @return
     */
    public static int fib2(int n){
        int prev = 0;
        int current = 1;
        int sum = 0;
        for (int i =0;i< n ;i++){
            sum = (prev + current)% 1000000007;
            prev = current;
            current = sum;
        }
        return prev;
    }

    public static void main(String[] args) {
        System.out.println(fib2(5));
    }



}
