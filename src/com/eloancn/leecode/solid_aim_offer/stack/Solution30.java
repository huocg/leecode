package com.eloancn.leecode.solid_aim_offer.stack;

import java.util.Stack;

/**
 * 定义栈的数据结构，请在该类型中实现一个能够得到栈的最小元素的 min 函数在该栈中，调用 min、push 及 pop 的时间复杂度都是 O(1)。
 *
 *  
 *
 * 示例:
 *
 * MinStack minStack = new MinStack();
 * minStack.push(-2);
 * minStack.push(0);
 * minStack.push(-3);
 * minStack.min();   --> 返回 -3.
 * minStack.pop();
 * minStack.top();      --> 返回 0.
 * minStack.min();   --> 返回 -2.
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/bao-han-minhan-shu-de-zhan-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution30 {
    private Stack<Integer> stack ;
    private Stack<Integer> minStack;
    /** initialize your data structure here. */
    public Solution30() {
        stack = new Stack();
        minStack = new Stack();
    }

    /**
     * 压入逻辑为判断最小stack 是不是为空或者是不是比最小栈顶元素小
     *
     * @param x
     */
    public void push(int x) {
        stack.push(x);
        if (minStack.isEmpty() || x<= minStack.peek()){
            minStack.push(x);
        }
    }
    // 判断最小站里面元素是不是和当前元素相等，如果是最小站也需要弹出
    public void pop() {
        if (minStack.peek().equals(stack.pop())){
            minStack.pop();
        }
    }

    public int top() {
        return stack.peek();
    }

    public int min() {
        return minStack.peek();
    }
}
