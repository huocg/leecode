package com.eloancn.leecode.linkedList;

public class ListNode {
	int value;
	ListNode next;
	
	public ListNode(int value) {
		super();
		this.value = value;
	}
	
}
