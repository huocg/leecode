package com.eloancn.leecode.linkedList;

public class Node {
	private Node nextNode;
	private int val;
	public Node getNextNode() {
		return nextNode;
	}
	public void setNextNode(Node nextNode) {
		this.nextNode = nextNode;
	}
	public int getVal() {
		return val;
	}
	public void setVal(int val) {
		this.val = val;
	}
	@Override
	public String toString() {
		return "Node [nextNode=" + nextNode + ", val=" + val + "]";
	}
	public Node(int val) {
		super();
		this.val = val;
	}
	
	
	

}
