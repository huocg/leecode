package com.eloancn.leecode.linkedList;


import org.junit.Test;

/**
 * 	反转链表
 * 	1->2->3
 * 	3->2->1
 * @author eloancn
 *
 */
public class RevertLinkedList {
	
	@Test
	public void runTest01() {
		Node node1 = new Node(1);
		Node node2 = new Node(2);
		Node node3 = new Node(3);
		node1.setNextNode(node2);
		node2.setNextNode(node3);
		revertFunc01(node1);
		formateLinkedList(node3);
		
	}
	public void revertFunc01(Node node) {
		Node beforeNode = null;
		Node currentNode = node;
		
		while(currentNode!=null) {
			Node nextNode = currentNode.getNextNode();
			currentNode.setNextNode(beforeNode);
			beforeNode= currentNode;
			currentNode = nextNode;
		}
	}
	public void formateLinkedList(Node node) {
		StringBuffer sb = new StringBuffer();
		while(node!=null) {
			sb.append(node.getVal()).append("->");
			node = node.getNextNode();
		}
		System.out.println(sb.toString());
	}
	// 使用递归方案
	public void revertFunc02(Node node) {
		
	}

}
