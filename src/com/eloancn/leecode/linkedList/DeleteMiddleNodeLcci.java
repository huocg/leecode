package com.eloancn.leecode.linkedList;
/**
 * 	删除中间节点
 * @author lihepeng
 *
 */
public class DeleteMiddleNodeLcci {
	public void deleteNode(Node node) {
		node.setVal(node.getNextNode().getVal());
		node.setNextNode(node.getNextNode().getNextNode());
    }
	
}
